#include <tuigraphics/Node.hpp>

void Node::SetParent(Node* parent)
{
    if (_parent != nullptr) {
        _parent->RemoveChild(this);
    }

    _parent = parent;
}

void Node::RemoveChild(const Node* node)
{
    for (auto& layer : _layers) {
        layer.second.erase(std::remove_if(layer.second.begin(), layer.second.end(), [node](const Node* candidate){
            return candidate == node;
        }), layer.second.end());
    }
    _childs.erase(std::remove_if(_childs.begin(), _childs.end(), [node](const Node* candidate){
        return candidate == node;
    }), _childs.end());
}

void Node::AppendChild(Node* node, int layer)
{
    if (node == nullptr) {
        return;
    }

    if (!_layers.count(layer)) {
        _layers.insert({ layer, {} });
    }

    node->SetParent(this);
    _layers.at(layer).push_back(node);
    _childs.push_back(node);
}

void Node::MoveBy(Vector2 offset)
{
    _position += offset;
}

CellGrid Node::ComputeGrid() const
{
    CellGrid computedGrid = _shape.GetGrid().GetGrid();

    for (const Node* child : _childs) {
        computedGrid.Merge(std::move(child->ComputeGrid()), child->GetPosition());
    }

    return computedGrid;
}
