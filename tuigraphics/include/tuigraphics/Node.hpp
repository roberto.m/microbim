#pragma once

#include <list>
#include <vector>
#include <memory> 
#include <optional>
#include <algorithm>
#include <iostream>

#include "shape/Shape.hpp"
#include "math.hpp"

class Node
{
    Shape _shape;
    Vector2 _position;
    Node* _parent = nullptr;

    std::map<int, std::list<Node*>> _layers;
    std::list<Node*> _childs;

public:
    Node() : _shape({ 0, 0 }) {}

    Node(Shape shape, Vector2 position) 
        : _shape(shape), _position(position) {
        auto size = shape.GetGrid().GetSize();
    }

    void SetShape(const Shape& shape)
    {
        _shape = shape;
    }

    void SetPosition(const Vector2& position)
    {
        _position = position;
    }

    const std::map<int, std::list<Node*>>& Layers() const
    {
        return _layers;
    }

    const Shape& GetShape() const
    {
        return _shape;
    }

    const Vector2& GetPosition() const
    {
        return _position;
    }

    const Node* Parent() const
    {
        return _parent;
    }

    const std::list<Node*>& Childs() const
    {
        return _childs;
    }

    CellGrid ComputeGrid() const;

    void SetParent(Node* parent);
    void RemoveChild(const Node* node);
    void AppendChild(Node* child, int layer = 0);
    void MoveBy(Vector2 offset);
};
