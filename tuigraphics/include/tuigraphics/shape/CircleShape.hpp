#pragma once

#include <cmath>
#include <vector>

#include <iostream>

#include "Shape.hpp"
#include "../Cell.hpp"
#include "../math.hpp"

class CircleShape : public Shape
{
    CellGrid _createCircleShapePixels(size_t radius, CellType cellType) const
    {
        Row row;
        Grid circle;

        row.assign(radius * 2, Cell());
        circle.assign(radius * 2, row);

        for (int y = -radius; y < (int)radius; y++) {

            int cosByRadius = std::sqrt(std::pow(radius, 2) - std::pow(y, 2));
            for (int x = -cosByRadius; x < cosByRadius; x++) {
                circle[radius + y][radius + x] = cellType;
            }

        }

        return CellGrid(circle);
    }

public:
    CircleShape(size_t radius, CellType cellType = CellType::BlockDot)
        : Shape(_createCircleShapePixels(radius, cellType)) {}
};
