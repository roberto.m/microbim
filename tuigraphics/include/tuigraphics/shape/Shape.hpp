#pragma once

#include "../math.hpp"
#include "../Grid.hpp"
#include "../types.hpp"

class Shape {
    CellGrid _grid;

public:
    Shape(Size size, CellType fillCell = CellType::Empty)
        : _grid(CellGrid(size, fillCell)) {}

    Shape(CellGrid cells)
        : _grid(cells) {}

    const CellGrid& GetGrid() const
    {
        return _grid;
    }

    const Size& GetSize() const
    {
        return _grid.GetSize();
    }
};
