#pragma once

#include "../types.hpp"
#include "../Cell.hpp"
#include "Shape.hpp"

class RectangleShape : public Shape
{
    CellGrid _createRectangleShapePixels(Size dimensions, CellType fillCell) const
    {
        Row row;
        Row rowInBetween;

        row.assign(dimensions.width, Cell(CellType::LineHorizontal));
        rowInBetween.assign(dimensions.width, fillCell);
        rowInBetween[0] = Cell(CellType::LineVertical);
        rowInBetween[rowInBetween.size() - 1] = Cell(CellType::LineVertical);

        Row topRow = row;
        topRow[0] = Cell(CellType::LineCornerTopLeft);
        topRow[topRow.size() - 1] = Cell(CellType::LineCornerTopRight);

        Row bottomRow = row;
        bottomRow[0] = Cell(CellType::LineCornerBottomLeft);
        bottomRow[bottomRow.size() - 1] = Cell(CellType::LineCornerBottomRight);

        Grid pixels;
        pixels.assign(dimensions.height, rowInBetween);
        pixels[0] = topRow;
        pixels[pixels.size() - 1] = bottomRow;

        return CellGrid(pixels);
    }

public:
    RectangleShape(Size dimensions, CellType fillWith = CellType::Empty) 
        : Shape(_createRectangleShapePixels(dimensions, fillWith)) {}
};