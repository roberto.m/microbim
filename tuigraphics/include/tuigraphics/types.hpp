#pragma once

#include <cstddef>
#include <cstdint>
#include <vector>

typedef struct {
    size_t width = 0;
    size_t height = 0;
} Size;
