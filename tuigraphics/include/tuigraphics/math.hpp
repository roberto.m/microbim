#pragma once

class Vector2
{
    int _x;
    int _y;
public:
    Vector2(int x, int y)
        : _x(x), _y(y) {}

    Vector2()
        : Vector2(Vector2::ZERO()) {}

    int x() const {
        return _x;
    }

    int y() const {
        return _y;
    }

    int& x() {
        return _x;
    }

    int& y() {
        return _y;
    }

    Vector2 operator-(const Vector2& other) const {
        auto x = this->x() - other.x();
        auto y = this->y() - other.y();
        return Vector2(x, y);
    }

    Vector2& operator-=(const Vector2& other) {
        this->_x -= other.x();
        this->_y -= other.y();
        return *this;
    }

    Vector2 operator+(const Vector2& other) const {
        auto x = this->x() + other.x();
        auto y = this->y() + other.y();
        return Vector2(x, y);
    }

    Vector2& operator+=(const Vector2& other) {
        this->_x += other.x();
        this->_y += other.y();
        return *this;
    }

    static Vector2 ZERO() {
        return Vector2(0, 0);
    }
};

