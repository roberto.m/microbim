#pragma once

#include <string>

#include "Node.hpp"

class Renderer
{
public:
    static std::string render(const Node& node)
    {
        CellGrid cellGrid = node.ComputeGrid();
        Grid grid = cellGrid.GetGrid();
        std::string string_repr;

        for (const auto& row : grid) {
            for (const auto& cell : row) {
                string_repr += cell.Character();
            }
            string_repr += "\n";
        }

        return string_repr;
    }
};