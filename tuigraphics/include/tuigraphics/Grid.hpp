#pragma once

#include <vector>
#include <string>
#include "types.hpp"
#include "Cell.hpp"

typedef std::vector<Cell> Row;
typedef std::vector<Row> Grid;

class CellGrid
{
    Grid _grid;
    Size _size;

    Grid _createGrid(const Size &size, CellType fillCell) const
    {
        Grid grid;
        Row row;

        row.assign(size.width, Cell(fillCell));
        grid.assign(size.height, row);

        return grid;
    }

public:
    CellGrid() = default;

    CellGrid(Size size, CellType fillCell = CellType::Empty)
        : _grid(_createGrid(size, fillCell)), _size(size) {}

    CellGrid(Grid grid) : _grid(grid) {
        if (grid.size() > 0 && grid[0].size() > 0) {
            _size = {
                .width = grid[0].size(),
                .height = grid.size()
            };
        }
    }

    void Merge(CellGrid other, Vector2 drawAt = Vector2::ZERO()) {
        if ((_grid.size() > 0 && _grid[0].size() > 0) == false) {
            return;
        }

        const Grid& otherGrid = other.GetGrid();

        for (size_t y = 0; y < _grid.size() && otherGrid.size() > y && _grid.size() > y + drawAt.y(); ++y) {
            for (size_t x = 0; x < _grid[0].size() && otherGrid[0].size() > x && _grid[0].size() > x + drawAt.x();  ++x) {
                if (std::string(otherGrid[y][x].Character()) != " ") {
                    _grid[y + drawAt.y()][x + drawAt.x()] = otherGrid[y][x];
                }
            }
        }
    }

    const Size& GetSize() const
    {
        return _size;
    }

    const Grid& GetGrid() const
    {
        return _grid;
    }
};
