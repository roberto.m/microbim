#pragma once

#include <map>

// https://en.wikipedia.org/wiki/Box-drawing_character
enum class CellType
{
    LineVertical,
    LineVerticalThick,
    LineDoubleVertical,
    LineHorizontal,
    LineHorizontalThick,
    LineDoubleHorizontal,
    LineCornerTopLeft,
    LineCornerTopRight,
    LineCornerBottomLeft,
    LineCornerBottomRight,
    LineDoubleCornerTopLeft,
    LineDoubleCornerTopRight,
    LineDoubleCornerBottomLeft,
    LineDoubleCornerBottomRight,
    LineCrossing,
    LineDoubleCrossing,
    LinePerpendicularLeft,
    LinePerpendicularRight,
    LinePerpendicularTop,
    LinePerpendicularBottom,
    LineDoublePerpendicularLeft,
    LineDoublePerpendicularRight,
    LineDoublePerpendicularTop,
    LineDoublePerpendicularBottom,
    LineInclined,
    BlockHorizontalSmall,
    BlockHorizontalHalf,
    BlockVerticalSmall,
    BlockVerticalHalf,
    BlockFull,
    BlockDiamond,
    BlockDot,
    Empty
};

static std::map<CellType, const char*> Cells = {
    {CellType::LineVertical,                    "│"},
    {CellType::LineVerticalThick,               ""},
	{CellType::LineDoubleVertical, 				"║"},
    {CellType::LineHorizontal,                  "─"},
    {CellType::LineHorizontalThick,             ""},
	{CellType::LineDoubleHorizontal, 			"═"},
    {CellType::LineCornerTopLeft,               "┌"},
    {CellType::LineCornerTopRight,              "┐"},
    {CellType::LineCornerBottomLeft,            "└"},
    {CellType::LineCornerBottomRight,           "┘"},
    {CellType::LineDoubleCornerTopLeft,         "╔"}, 
    {CellType::LineDoubleCornerTopRight,        "╗"},
    {CellType::LineDoubleCornerBottomLeft,      "╚"},
    {CellType::LineDoubleCornerBottomRight,     "╝"},
    {CellType::LineCrossing,                    "┼"},
    {CellType::LineDoubleCrossing,              "╬"},
    {CellType::LinePerpendicularLeft,           "┥"},
    {CellType::LinePerpendicularRight,          "┝"},
    {CellType::LinePerpendicularTop,            "┻"},
    {CellType::LinePerpendicularBottom,         "┯"},
    {CellType::LineDoublePerpendicularLeft,     "╣"},
    {CellType::LineDoublePerpendicularRight,    "╠"},
    {CellType::LineDoublePerpendicularTop,      "╦"},
    {CellType::LineDoublePerpendicularBottom,   "╩"},
    {CellType::LineInclined,                    "╲"},
    {CellType::BlockHorizontalSmall,            "▂"}, 
    {CellType::BlockHorizontalHalf,             "▅"},
    {CellType::BlockVerticalSmall,              "▎"},
    {CellType::BlockVerticalHalf,               "▌"},
    {CellType::BlockFull,                       "▇"},
    {CellType::BlockDiamond,                    "◆"},
    {CellType::BlockDot,                        "●"},
    {CellType::Empty,                           " "},
};

class Cell {
    const char* _character;

public:
    Cell(CellType cellType)
        : _character(Cells.at(cellType)) {}

    Cell()
        : _character(Cells.at(CellType::Empty)) {}

    const char* Character() const {
        return _character;
    };

    virtual void Blend(const Cell& other) {
    };
};
