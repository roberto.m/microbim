#pragma once

#include <tuigraphics/Node.hpp>

namespace Collisions
{

bool CheckCollision(const Node& firstNode, const Node& secondNode)
{
    auto checkCollisions = [](auto firstPosition, auto secondPosition, auto firstSize, auto secondSize) {
        int x1 = firstPosition.x();
        int x2 = secondPosition.x();
        int y1 = firstPosition.y();
        int y2 = secondPosition.y();

        int w1 = firstSize.width;
        int w2 = secondSize.width;
        int h1 = firstSize.height;
        int h2 = secondSize.height;

        bool x2Insidex1h1 = x1 <= x2 && x1 + w1 >= x2;
        if (x2Insidex1h1) {
            bool bottomLeftCornerOrTraverses = y2 <= y1 && y2 + h2 >= y1;
            bool topLeftCornerOnly = y2 >= y1 && y2 <= y1 + h1;
            return bottomLeftCornerOrTraverses || topLeftCornerOnly;
        }

        return false;
    };

    auto p1 = firstNode.GetPosition();
    auto p2 = secondNode.GetPosition();
    auto s1 = firstNode.GetShape().GetSize();
    auto s2 = secondNode.GetShape().GetSize();

    return checkCollisions(p1, p2, s1, s2)
        || checkCollisions(p2, p1, s2, s1);
}

}