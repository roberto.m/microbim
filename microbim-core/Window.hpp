#pragma once

#include <tuigraphics/Node.hpp>
#include <tuigraphics/shape/Shape.hpp>
#include <tuigraphics/shape/CircleShape.hpp>
#include <tuigraphics/shape/RectangleShape.hpp>

#include "Material.hpp"
#include "Billable.hpp"

class Window : public Node, public Billable
{
public:
    Window() = default;

    virtual Bill GetBill() const {
        return { 0, 0 };
    }
};

class RectangularWindow : public Window
{
public:
    RectangularWindow(Size windowSize, Vector2 position)
    {
        SetShape(RectangleShape(windowSize, MaterialCell.at(Material::Glass)));
        SetPosition(position);
    }

    Bill GetBill() const override
    {
        return {
            .price = 300,
            .manufacturingDays = 3
        };
    }
};

class CircularWindow : public Window
{
public:
    CircularWindow(size_t windowRadius, Vector2 position)
    {
        SetShape(CircleShape(windowRadius, MaterialCell.at(Material::Glass)));
        SetPosition(position);
    }

    Bill GetBill() const override
    {
        return {
            .price = 400,
            .manufacturingDays = 4
        };
    }
};
