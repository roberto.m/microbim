#pragma once

#include <map>

#include <tuigraphics/Cell.hpp>

enum class Material
{
    Wood,
    Concrete,
    MagicAir,
    Glass
};

static std::map<Material, CellType> MaterialCell = {
    { Material::Wood, CellType::BlockFull },
    { Material::Concrete, CellType::BlockDiamond },
    { Material::MagicAir, CellType::Empty },
    { Material::Glass, CellType::LineInclined },
};
