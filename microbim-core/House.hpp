#pragma once

#include <list>

#include <tuigraphics/Node.hpp>
#include <tuigraphics/types.hpp>
#include <tuigraphics/shape/Shape.hpp>

#include <memory>

#include "Floor.hpp"
#include "Roof.hpp"
#include "Billable.hpp"

class House : public Node, public Billable {
    std::list<Floor> _floors;
    GroundFloor _groundFloor;
    Roof _roof;

    void _shiftFloors(int shiftBy);

public:
    House(size_t houseWidth, Material groundFloorMaterial, size_t groundFloorHeight)
        :  Node(Shape({houseWidth, groundFloorHeight}), Vector2::ZERO()),
        _groundFloor({houseWidth, groundFloorHeight}, Vector2::ZERO(), groundFloorMaterial)
    {
        AppendChild(&_groundFloor);
    }

    void AddFloor(Material wallMaterial, size_t floorHeight);
    void AddDoor(Material dootMaterial, Size doorSize, int doorX);

    Size GetSize() const
    {
        return GetShape().GetSize();
    }

    const std::list<Floor>& Floors() const
    {
        return _floors;
    }

    std::list<Floor>& Floors()
    {
        return _floors;
    }

    virtual Bill GetBill() const override
    {
        Bill bill;

        auto setBill = [&](const Bill& otherBill) {
            bill.price += otherBill.price;

            if (otherBill.manufacturingDays > bill.manufacturingDays) {
                bill.manufacturingDays = otherBill.manufacturingDays;
            }
        };

        for (const auto& floor : _floors) {
            setBill(floor.GetBill());
        }

        setBill(_roof.GetBill());
        setBill(_groundFloor.GetBill());

        return bill;
    }
};
