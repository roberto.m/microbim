#pragma once

#include <list>
#include <vector>

#include <tuigraphics/Node.hpp>
#include <tuigraphics/math.hpp>
#include <tuigraphics/types.hpp>
#include <tuigraphics/shape/RectangleShape.hpp>

#include "Window.hpp"
#include "Door.hpp"
#include "Material.hpp"
#include "Billable.hpp"

class Floor : public Node, public Billable
{
    std::list<Window> _windows;

public:
    Floor(Size floorSize, Vector2 position, Material wallMaterial)
        : Node(RectangleShape(floorSize, MaterialCell.at(wallMaterial)), position)
    {}

    void AddWindow(Window window)
    {
        Size floorSize = GetShape().GetSize();
        size_t step = floorSize.width / (_windows.size() + 2);

        _windows.push_back(window);

        int xPos = step;
        for (auto& window : _windows) {
            int windowX = xPos - (window.GetShape().GetSize().width / 2);
            window.SetPosition(Vector2(windowX, window.GetPosition().y()));
            xPos += step;
        }

        AppendChild(&(_windows.back()));
    }

    virtual Bill GetBill() const override
    {
        Bill bill;

        auto setBill = [&](const Bill& otherBill) {
            bill.price += otherBill.price;

            if (otherBill.manufacturingDays > bill.manufacturingDays) {
                bill.manufacturingDays = otherBill.manufacturingDays;
            }
        };

        for (const auto& window : _windows) {
            setBill(window.GetBill());
        }

        return {
            .price = 3000,
            .manufacturingDays = 10
        };
    }
};

class GroundFloor : public Floor
{
    std::list<Door> _doors;

public:
    GroundFloor(Size floorSize, Vector2 position, Material wallMaterial)
        : Floor(floorSize, position, wallMaterial)
    {}

    void AddDoor(Material doorMaterial, int doorX, Size doorSize)
    {
        int doorY = GetShape().GetSize().height - doorSize.height;
        _doors.push_back(Door(doorSize, Vector2(doorX, doorY), doorMaterial));
        AppendChild(&_doors.back());
    }

    virtual Bill GetBill() const override
    {
        Bill bill = Floor::GetBill();

        auto setBill = [&](const Bill& otherBill) {
            bill.price += otherBill.price;

            if (otherBill.manufacturingDays > bill.manufacturingDays) {
                bill.manufacturingDays = otherBill.manufacturingDays;
            }
        };

        for (const auto& door : _doors) {
            setBill(door.GetBill());
        }

        return bill;
    }
};
