#pragma once

typedef struct Bill
{
    float price = 0;
    int manufacturingDays = 0;
} Bill;

class Billable
{
    virtual Bill GetBill() const = 0;
};
