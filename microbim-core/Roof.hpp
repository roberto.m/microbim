#pragma once

#include <tuigraphics/Node.hpp>

#include "Billable.hpp"

class Roof : public Node, public Billable
{
public: 
    Roof() = default;

    virtual Bill GetBill() const override
    {
        return {
            .price = 3000,
            .manufacturingDays = 20
        };
    }
};
