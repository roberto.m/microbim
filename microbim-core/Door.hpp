#pragma once

#include <tuigraphics/Node.hpp>
#include <tuigraphics/shape/RectangleShape.hpp>

#include "Billable.hpp"

class Door : public Node, public Billable
{

public:
    Door(Size doorSize, Vector2 position, Material material)
        : Node(RectangleShape(doorSize, MaterialCell.at(material)), position)
    {}

    Bill GetBill() const {
        return {
            .price = 400,
            .manufacturingDays = 10
        };
    }
};
