#include "House.hpp"
#include "Floor.hpp"
#include "Door.hpp"

#include <utility>

void House::AddFloor(Material wallMaterial, size_t floorHeight)
{
    Size houseSize = GetShape().GetSize();

    Size floorSize = {
        .width = houseSize.width,
        .height = floorHeight
    };

    houseSize.height += floorHeight;

    SetShape(Shape(houseSize));
    _shiftFloors(floorHeight - 1);

    Floor floor(floorSize, Vector2::ZERO(), wallMaterial);
    _floors.push_back(floor);
    AppendChild(&_floors.back());
}

void House::_shiftFloors(int shiftBy)
{
    _groundFloor.MoveBy(Vector2(0, shiftBy));
    for (auto& floor : _floors) {
        floor.MoveBy(Vector2(0, shiftBy));
    }
}

void House::AddDoor(Material doorMaterial, Size doorSize, int doorX)
{
    _groundFloor.AddDoor(doorMaterial, doorX, doorSize);
}
