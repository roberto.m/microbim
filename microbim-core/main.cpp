#include <array>
#include <cstdio>
#include <cassert>

#include <tuigraphics/math.hpp>
#include <tuigraphics/Node.hpp>
#include <tuigraphics/Renderer.hpp>
#include <tuigraphics/shape/CircleShape.hpp>
#include <tuigraphics/shape/RectangleShape.hpp>

#include "Collisions.hpp"
#include "House.hpp"

static int UserReadInteger(const std::string& tellUser)
{
    int actuallyInt;
    std::string maybeInt;

    while (true) {
        std::cout << tellUser << " ";
        std::cin >> maybeInt;
        try {
            actuallyInt = std::stoi(maybeInt);;
            break;
        } catch (...) {
            std::cout << "Utilizza un valore numerico...\n";
        }
    }

    return actuallyInt;
}

static int UserSelectInRange(int min, int max, const std::string& tellUser)
{
    assert(min < max && "Min is greater than max, values must be swapped\n");

    int userAnswer = min - 1;

    while (userAnswer < min || userAnswer > max) {
        userAnswer = UserReadInteger(tellUser);
        if (userAnswer < min || userAnswer > max) {
            std::cout << "Inserisci un numero tra quelli possibili:\n";
        }
    }

    return userAnswer;
}

static std::string materialChoice = "1) Legno\n"
                                    "2) Cemento\n"
                                    "3) Aria?\n";

int main(void)
{
    std::system("clear");
    int minHouseWidth = 40;
    int maxHouseWidth = 200;
    int floorHeight = 10;
    int houseWidth = UserSelectInRange(minHouseWidth, maxHouseWidth,
        "Benvenuto nel MicroBIM, seleziona la larghezza della casa (" + std::to_string(minHouseWidth) + "-" + std::to_string(maxHouseWidth) + "):");

    std::array<Material, 3> materials = {
        Material::Wood,
        Material::Concrete,
        Material::MagicAir
    };

    int materialId = UserSelectInRange(1, materials.size(),
        materialChoice + "Seleziona il materiale della casa:") - 1;

    Material material = materials[materialId];

    House house(houseWidth, material, 10);
    CircularWindow defaultWindow(3, Vector2(0, floorHeight / 3));
    house.AddDoor(Material::Concrete, {5, 5}, (houseWidth / 2) - 3);

    int nFloors = UserReadInteger("Numero di piani:");

    for (int i = 1; i <= nFloors; i++) {
        int nWindows = UserReadInteger("Numero di finestre per il piano " + std::to_string(i) + ":");
        house.AddFloor(material, floorHeight);

        for (int j = 0; j < nWindows; ++j) {
            auto& floor = house.Floors().back();
            floor.AddWindow(defaultWindow);
        }
    }

    std::system("clear");
    std::cout << Renderer::render(house) << "\n";

    auto bill = house.GetBill();
    std::cout << "Prezzo finale: " << bill.price << "€\n";
    std::cout << "Giorni per la fabbricazione: " << bill.manufacturingDays << "\n";

    return 0;
}
